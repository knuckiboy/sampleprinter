package consoleprinter;

import java.util.Scanner;

import consoleprinter.text.IText;
import consoleprinter.text.impl.CText;
import consoleprinter.text.impl.XText;
import consoleprinter.text.impl.YText;
import consoleprinter.text.impl.ZText;

public class Main {
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		IText textToPrint;
		System.out.print("User Input: ");
		String text = input.next();
		int size = input.nextInt();

		System.out.println("Printing: ");
		switch (text.toLowerCase()) {
		case "x":
			textToPrint = new XText();
			textToPrint.createText(size);
			break;
		case "y":
			textToPrint = new YText();
			textToPrint.createText(size);
			break;
		case "z":
			textToPrint = new ZText();
			textToPrint.createText(size);
			break;
		case "c":
			textToPrint = new CText();
			textToPrint.createText(size);
			break;
		}

		input.close();

	}

}
