package consoleprinter.text.impl;

import consoleprinter.text.IText;

public class YText implements IText {

	@Override
	public void createText(int size) {
		// TODO Auto-generated method stub
		int count = size / 2;
		for (int i = 0; i < size; i++) {
			int j = size - 1 - i;
			for (int k = 0; k < size; k++) {
				if (i > count) {
					if (k == count) {
						System.out.print("*");
					} else {
						System.out.print(" ");
					}

				} else {
					if (k == i || k == j)
						System.out.print("*");
					else
						System.out.print(" ");
				}
			}
			System.out.println("");
		}
	}

}
